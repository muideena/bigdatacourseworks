
--Query 1 (The price_id and Avg_price for each security with an average trading volume greater than 300mn)
SELECT price_id, AVG(close) AS Avg_Price, AVG(volume) AS Avg_Vol
FROM equity_prices
GROUP BY price_id
HAVING Avg_Vol > 300000000
ORDER BY Avg_Vol DESC;

--Query 2 (The average limit for each trader on USD trades)
SELECT trader_name, round(AVG(limit_amount),1) AS Avg_Limit
FROM trader_static
LEFT JOIN trader_limits ON trader_static.trader_id = trader_limits.trader_id
WHERE currency = 'USD'
GROUP BY trader_name;