# UCL IFT - Big Data Coursework

## Coursework One
[Published 14/11/2021 00:00:01]
### Objectives
Objective of Coursework One is to demonstrate familiarity with both SQL and NoSQL MongoDB syntax for querying data.
In addition, the student will also show that she/he has understood the Git workflow by submitting the main queries into Bitbucket.

### Sql and NoSql database for coursework one
In order to complete each coursework, the student must use the databases stored in [iftcoursework2021](https://bitbucket.org/uceslc0/iftcoursework2021/src/master/) under folder 000.DataBases. The SQL database can be accessed by using SQLite Portable Browser or any other type od SQLite User Interface and will be under the folder 000.DataBases/SQL/Equity. 

The NoSQL data, provided in a json file (CourseworkOne.json), requires loading into MongoDB. In order to load the json file on MongoDB, please follow these steps:
1. Open MongoDb Shell;
2. In MongoDB shell perform the following actions:

```
use Equity
db.createCollection("CourseworkOne")
db.CourseworkOne.insert() // copy and paste the full json file between the rounded brackets.
```

### Requirements
The student should develop at least two queries for each SQL and NoSQL database. The complexity and sophistication of the query is left at student discretion.


#### Code Submission
For each coursework submission code must be submitted in bitbucket. The repository for submission is (iftcoursework2021)[https://bitbucket.org/uceslc0/iftcoursework2021/src/master/].

In order to submit the coursework, the student needs to follow the following steps:
1. Clone the repository;
2. create a new branch;
3. add your own developments in a dedicated folder;
4. the dedicated folder has to be placed under "./iftcoursework2021/" and is structured as following:

```
NameSurname
    |_1.CourseworkOne
    |__.gitkeep
    |_2.CourseworkTwo
    |__.gitkeep
    |_3.CourseworkThree
    |__.gitkeep
```
*Please see example under this directory for folder name called "LucaCocconcelli"*

Two files for Coursework One submission will then be placed under 1.CourseworkOne with following naming conventions:
* "1.SQLQueries.txt"
* "2.NoSQLQueries.js"

Once this is completed, in order to submit the code to Bitbucket:
1. check git status;
2. if the local repository is behind the master, please perform a git pull;
3. add & commit;
4. push the origin to the branch;
5. go to bitbucket and create a pull request by merging your branch into the master.

Please note, the iftcoursework2021 will go back to read only mode at the End of Day for Coursework One submission (23:59 GMT). After this, if you'd like to submit your code to bitbucket, please send an email containing the folder (zipped) to l.cocconcelli@ucl.ac.uk

#### Report
Along side to code developments, the student must write a report (500 Words max) that will be submitted in Turnitin according to the deadlines specified at the top of this page. The report should be structured with:
1. Short Introduction;
2. Brief description of SQL vs NoSQL database;
3. Query documentation: for each query provided, please list: 
   1. background (why I need this query), 
   2. aims (what I will achieve with my query), 
   3. approach (how I addressed the challenge and what each statement does), 
   4. output (describe main results obtained).
4. Conclusion: conclude the work and summary of key findings.
